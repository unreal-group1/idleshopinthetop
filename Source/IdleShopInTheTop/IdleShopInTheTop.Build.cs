// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class IdleShopInTheTop : ModuleRules
{
	public IdleShopInTheTop(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] 
		{ 
			"Core", 
			"CoreUObject", 
			"Engine", 
			"InputCore", 
			"NavigationSystem", 
			"AIModule", 
			"Niagara", 
			"EnhancedInput" 
		});

        PublicIncludePaths.AddRange(new string[] {
            "IdleShopInTheTop/UI/PopupHints/Components/",
            "IdleShopInTheTop/UI/PopupHints/Interfaces/",
            "IdleShopInTheTop/UI/PopupHints/",
            "IdleShopInTheTop/UI/Count/",
            "IdleShopInTheTop/UI/",
            "IdleShopInTheTop/Core/Components/",
            "IdleShopInTheTop/Core/Interfaces/",
            "IdleShopInTheTop/Core/Interfaces/Interact/",
            "IdleShopInTheTop/Core/",
            "IdleShopInTheTop/FunctionLibraries/",
            "IdleShopInTheTop/Game/",
            "IdleShopInTheTop/"
        });
    }
}
