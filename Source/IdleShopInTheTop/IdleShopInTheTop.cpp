// Copyright Epic Games, Inc. All Rights Reserved.

#include "IdleShopInTheTop.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, IdleShopInTheTop, "IdleShopInTheTop" );

DEFINE_LOG_CATEGORY(LogIdleShopInTheTop)
 