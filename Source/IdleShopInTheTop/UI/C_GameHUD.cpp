// SB Pavlov P.A.

#include "C_GameHUD.h"
#include "CW_PopupHint.h"
#include "CW_Game.h"

#include "Blueprint/UserWidget.h"

void AC_GameHUD::BeginPlay()
{

}

void AC_GameHUD::Init()
{
	if (GameWidgetClass
		&& GetWorld())
	{
		GameWidget = CreateWidget<UCW_Game>(GetWorld(), GameWidgetClass, FName(TEXT("GameWidget")));
		if (GameWidget)
		{
			GameWidget->AddToViewport();
			GameWidget->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}
