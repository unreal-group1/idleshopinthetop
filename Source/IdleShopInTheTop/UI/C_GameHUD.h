// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "C_GameHUD.generated.h"

class UCW_Game;

UCLASS()
class IDLESHOPINTHETOP_API AC_GameHUD : public AHUD
{
	GENERATED_BODY()
	
	public:

	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	TSubclassOf<UCW_Game> GameWidgetClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	UCW_Game* GameWidget;

	UFUNCTION(BlueprintCallable)
	void Init();
};
