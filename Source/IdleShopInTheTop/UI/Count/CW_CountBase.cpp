// SB Pavlov P.A.

#include "CW_CountBase.h"

#include "Components/TextBlock.h"

void UCW_CountBase::NativeOnInitialized()
{
}

void UCW_CountBase::UpdateCurrentCount(int32 CurrentCount)
{
	TB_CurrentCount->SetText(FText::FromString(FString::FromInt(CurrentCount)));
}
