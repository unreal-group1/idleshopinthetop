// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "CW_CountBase.h"
#include "CW_CountProduct.generated.h"

class AC_Product;

UCLASS()
class IDLESHOPINTHETOP_API UCW_CountProduct : public UCW_CountBase
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Product)
	TSubclassOf<AC_Product> ProductClass;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TB_Slash;
	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TB_MaxProductsCount;

	private:
	virtual void NativeOnInitialized() override;
	virtual void UpdateCurrentCount(int32 CurrentCount) override;
	UFUNCTION()
	virtual void UpdateCurrentCountProduct(TSubclassOf<AC_Product> TargetProductClass, int32 CurrentCount);
	UFUNCTION()
	virtual void UpdateMaxCountProduct(TSubclassOf<AC_Product> TargetProductClass, int32 MaxCountProducts);
};
