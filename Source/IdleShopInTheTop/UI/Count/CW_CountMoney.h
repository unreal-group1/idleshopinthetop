// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "CW_CountBase.h"
#include "CW_CountMoney.generated.h"

/**
 * 
 */
UCLASS()
class IDLESHOPINTHETOP_API UCW_CountMoney : public UCW_CountBase
{
	GENERATED_BODY()
	
	private:
	virtual void NativeOnInitialized() override;
	virtual void UpdateCurrentCount(int32 CurrentCount) override;
};
