// SB Pavlov P.A.

#include "CW_CountProduct.h"
#include "CI_InteractableWithBackpack.h"
#include "IdleShopInTheTopCharacter.h"
#include "C_BackpackComponent.h"
#include "CFL_Product.h"
#include "C_Product.h"

#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

void UCW_CountProduct::NativeOnInitialized()
{
	if (auto World = GetWorld())
	{
		auto Character = Cast<AIdleShopInTheTopCharacter>(UGameplayStatics::GetPlayerCharacter(World, 0));
		if (!Character)
		{
			return;
		}
		auto ActorWithBackpack = Cast<ICI_InteractableWithBackpack>(Character);

		if (ActorWithBackpack)
		{
			auto Backpack = ActorWithBackpack->Execute_GetBackpackComponent(Character);

			if (Backpack)
			{
				auto Product = Backpack->FindProduct(ProductClass);
				if (Product.ProductClass)
				{
					Backpack->OnChangeCurrentCount.AddDynamic(this,
						&UCW_CountProduct::UpdateCurrentCountProduct);
					Backpack->OnChangeMaxCount.AddDynamic(this,
						&UCW_CountProduct::UpdateMaxCountProduct);

					TB_CurrentCount->SetText(
						FText::FromString(
							FString::FromInt(
								Backpack->GetCurrentCount(Product.ProductClass)
							)
						)
					);
					TB_MaxProductsCount->SetText(
						FText::FromString(
							FString::FromInt(
								Backpack->GetMaxCount(Product.ProductClass)
							)
						)
					);
				}
			}
		}
	}
}

void UCW_CountProduct::UpdateCurrentCount(int32 CurrentCount)
{
	Super::UpdateCurrentCount(CurrentCount);
}

void UCW_CountProduct::UpdateCurrentCountProduct(TSubclassOf<AC_Product> TargetProductClass, 
	int32 CurrentCount)
{
	UpdateCurrentCount(CurrentCount);
}

void UCW_CountProduct::UpdateMaxCountProduct(TSubclassOf<AC_Product> TargetProductClass,
	int32 MaxCountProducts)
{
	TB_MaxProductsCount->SetText(FText::FromString(FString::FromInt(MaxCountProducts)));
}
