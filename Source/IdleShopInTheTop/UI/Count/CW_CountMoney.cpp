// SB Pavlov P.A.

#include "CW_CountMoney.h"
#include "IdleShopInTheTopCharacter.h"
#include "C_WalletComponent.h"

#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

void UCW_CountMoney::NativeOnInitialized()
{
	if (auto World = GetWorld())
	{
		auto PlayerCharacter = Cast<AIdleShopInTheTopCharacter>(
			UGameplayStatics::GetPlayerCharacter(World, 0)
		);

		if (PlayerCharacter)
		{
			auto Wallet = PlayerCharacter->WalletComponent;

			if (Wallet)
			{
				Wallet->OnChangeCurrentMoney.AddDynamic(this,
					&UCW_CountMoney::UpdateCurrentCount);

				TB_CurrentCount->SetText(FText::FromString(FString::FromInt(Wallet->GetCurrentCountMoney())));
			}
		}
	}
}

void UCW_CountMoney::UpdateCurrentCount(int32 CurrentCount)
{
	Super::UpdateCurrentCount(CurrentCount);
}
