// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CW_CountBase.generated.h"

/**
 * 
 */
UCLASS()
class IDLESHOPINTHETOP_API UCW_CountBase : public UUserWidget
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	class UHorizontalBox* HB_Count;

	UPROPERTY(meta = (BindWidget))
	class UImage* I_Object;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* TB_CurrentCount;

	protected:
	UFUNCTION()
	virtual void NativeOnInitialized() override;
	UFUNCTION()
	virtual void UpdateCurrentCount(int32 CurrentCount);
};
