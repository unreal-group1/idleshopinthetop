// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "C_PopupHintComponent.generated.h"

/**
 * ����������� ��������� ��� �������
 */
UCLASS()
class IDLESHOPINTHETOP_API UC_PopupHintComponent : public UWidgetComponent
{
	GENERATED_BODY()
	
	public:
	virtual void InitWidget() override;
};
