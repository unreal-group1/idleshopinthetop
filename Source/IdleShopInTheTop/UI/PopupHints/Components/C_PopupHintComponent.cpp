// SB Pavlov P.A.

#include "C_PopupHintComponent.h"
#include "C_Product.h"
#include "C_Shelf.h"
#include "CW_PopupHint.h"
#include "CW_PopupHintWithButton.h"

void UC_PopupHintComponent::InitWidget()
{
	Super::InitWidget();

	if (auto Product = Cast<AC_Product>(GetOwner()))
	{
		UCW_PopupHint* PopupHint = Cast<UCW_PopupHint>(GetUserWidgetObject());
		if (PopupHint)
		{
			PopupHint->Product = Product;
			PopupHint->Init();
		}
	}

	if (auto Shelf = Cast<AC_Shelf>(GetOwner()))
	{
		UCW_PopupHintWithButton* PopupHint = Cast<UCW_PopupHintWithButton>(GetUserWidgetObject());
		if (PopupHint)
		{
			PopupHint->Shelf = Shelf;
			PopupHint->Init();
		}
	}

	bDrawAtDesiredSize = true;
}
