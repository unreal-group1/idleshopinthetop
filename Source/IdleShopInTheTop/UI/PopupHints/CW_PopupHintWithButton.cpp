// SB Pavlov P.A.

#include "CW_PopupHintWithButton.h"
#include "C_Shelf.h"
#include "IdleShopInTheTopCharacter.h"

#include "Components/TextBlock.h"
#include "Components/Button.h"

void UCW_PopupHintWithButton::Init()
{
    Super::Init();

    if (Shelf)
    {
        if (Shelf->StaticMeshComponent)
        {
            Shelf->StaticMeshComponent->OnClicked.AddDynamic(this, &UCW_BasePopupHint::Clicked_Implementation);
            Shelf->StaticMeshComponent->OnReleased.AddDynamic(this, &UCW_BasePopupHint::Released_Implementation);
            Shelf->StaticMeshComponent->OnInputTouchEnter.AddDynamic(this, &UCW_BasePopupHint::InputTouchBegin_Implementation);
            Shelf->StaticMeshComponent->OnInputTouchLeave.AddDynamic(this, &UCW_BasePopupHint::InputTouchEnd_Implementation);
        }
        Shelf->OnChangeCurrentCostUpgrade.AddDynamic(this, &UCW_PopupHintWithButton::UpdateHintText);

        ICI_InfoInHint* InfoInHint = Cast<ICI_InfoInHint>(Shelf);
        if (InfoInHint)
        {
            if (TB_TextName)
            {
                TB_TextName->SetAutoWrapText(true);
                TB_TextName->SetText(InfoInHint->Execute_GetNameForHint(Shelf));
            }
            if (TB_TextHint)
            {
                TB_TextHint->SetAutoWrapText(true);
                TB_TextHint->SetText(InfoInHint->Execute_GetDescriptionForHint(Shelf));
            }
        }

        B_Ok->OnClicked.AddDynamic(this, &UCW_PopupHintWithButton::OkClick);
        B_No->OnClicked.AddDynamic(this, &UCW_PopupHintWithButton::NoClick);
    }

    bIsClickable = true;
}

void UCW_PopupHintWithButton::OkClick()
{
    ICI_Upgraded* Upgraded = Cast<ICI_Upgraded>(Shelf);
    if (Upgraded)
    {
        if (GetWorld()
            && GetWorld()->GetFirstLocalPlayerFromController())
        {
            auto Character = Cast<AIdleShopInTheTopCharacter>(GetWorld()->GetFirstPlayerController()->GetCharacter());
            UE_LOG(LogTemp, Warning, TEXT("%s Cost upgrade: %i"), *Character->GetName(), Shelf->CurrentCostUpgrade);
            Upgraded->Execute_Upgrade(Shelf, Character, Shelf->CurrentCostUpgrade);
        }
    }
    Execute_Hide(this);
}

void UCW_PopupHintWithButton::NoClick()
{
    Execute_Hide(this);
}

void UCW_PopupHintWithButton::UpdateHintText(AC_Shelf* ShelfUpdated, int32 MaxCount)
{
    ICI_InfoInHint* InfoInHint = Cast<ICI_InfoInHint>(ShelfUpdated);
    if (InfoInHint)
    {
        if (TB_TextHint)
        {
            TB_TextHint->SetAutoWrapText(true);
            TB_TextHint->SetText(InfoInHint->Execute_GetDescriptionForHint(ShelfUpdated));
        }
    }
}
