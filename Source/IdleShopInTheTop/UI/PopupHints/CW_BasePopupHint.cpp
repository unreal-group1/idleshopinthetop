// SB Pavlov P.A.

#include "CW_BasePopupHint.h"

bool UCW_BasePopupHint::Initialize()
{
    auto Result = Super::Initialize();
    Init();

    return Result;
}

void UCW_BasePopupHint::Init()
{
    Hide_Implementation();
}

void UCW_BasePopupHint::Show_Implementation()
{
    SetVisibility(ESlateVisibility::Visible);
}

void UCW_BasePopupHint::Hide_Implementation()
{
    SetVisibility(ESlateVisibility::Hidden);
}

void UCW_BasePopupHint::BeginCursorOver_Implementation(UPrimitiveComponent* TouchedComponent)
{
    if (!bIsClickable)
    {
        Show_Implementation();
        UE_LOG(LogTemp, Error, TEXT("Visibility without click"));
    }
}

void UCW_BasePopupHint::EndCursorOver_Implementation(UPrimitiveComponent* TouchedComponent)
{
    if (!bIsClickable)
    {
        StartHideTimer();
        UE_LOG(LogTemp, Error, TEXT("Hide without click"));
    }
}

void UCW_BasePopupHint::BeginTouchOver_Implementation(ETouchIndex::Type FingerIndex,
    UPrimitiveComponent* TouchedComponent)
{
    if (!bIsClickable)
    {
        Show_Implementation();
    }
}

void UCW_BasePopupHint::EndTouchOver_Implementation(ETouchIndex::Type FingerIndex,
    UPrimitiveComponent* TouchedComponent)
{
    if (!bIsClickable)
    {
        StartHideTimer();
    }
}

void UCW_BasePopupHint::Clicked_Implementation(UPrimitiveComponent* TouchedComponent, 
    FKey ButtonPressed)
{
    if (bIsClickable)
    {
        Show_Implementation();
        UE_LOG(LogTemp, Error, TEXT("Visibility with click"));
    }
}

void UCW_BasePopupHint::Released_Implementation(UPrimitiveComponent* TouchedComponent, 
    FKey ButtonPressed)
{
    if (bIsClickable)
    {
        StartHideTimer();
        UE_LOG(LogTemp, Error, TEXT("Hide with click"));
    }
}

void UCW_BasePopupHint::InputTouchBegin_Implementation(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
    if (bIsClickable)
    {
        Show_Implementation();
    }
}

void UCW_BasePopupHint::InputTouchEnd_Implementation(ETouchIndex::Type FingerIndex, UPrimitiveComponent* TouchedComponent)
{
    if (bIsClickable)
    {
        StartHideTimer();
    }
}

void UCW_BasePopupHint::StartHideTimer()
{
    if (GetWorld())
    {
        GetWorld()->GetTimerManager().SetTimer(PopupHintTimer,
            this,
            &UCW_BasePopupHint::Hide_Implementation,
            DelayHidePopupHint,
            false);
    }
}