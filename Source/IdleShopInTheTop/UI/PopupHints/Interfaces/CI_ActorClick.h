// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CI_ActorClick.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCI_ActorClick : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IDLESHOPINTHETOP_API ICI_ActorClick
{
	GENERATED_BODY()

public:
/**
* ��������� � ������� AActor OnClick
*/
UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
void Clicked(UPrimitiveComponent* TouchedActor, FKey ButtonPressed);
/**
* ��������� � ������� AActor OnReleased
*/
UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
void Released(UPrimitiveComponent* TouchedActor, FKey ButtonPressed);
/**
* ��������� � ������� AActor OnInputTouchBegin
*/
UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
void InputTouchBegin(ETouchIndex::Type FingerIndex,
	UPrimitiveComponent* TouchedComponent);
/**
* ��������� � ������� AActor OnInputTouchEnd
*/
UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
void InputTouchEnd(ETouchIndex::Type FingerIndex,
	UPrimitiveComponent* TouchedComponent);
};
