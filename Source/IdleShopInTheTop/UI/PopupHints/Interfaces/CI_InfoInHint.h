// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "CI_ActorOver.h"
#include "CI_InfoInHint.generated.h"

UINTERFACE(MinimalAPI, meta = (Blueprintable))
class UCI_InfoInHint : public UInterface
{
	GENERATED_BODY()
};

class IDLESHOPINTHETOP_API ICI_InfoInHint
{
	GENERATED_BODY()

public:
	/**
	* ���������� ���
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetNameForHint();
	/**
	* ���������� �������� 
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FText GetDescriptionForHint();
};
