//SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InputCoreTypes.h"

#include "CI_ActorOver.h"
#include "CI_PopupHintable.generated.h"

UINTERFACE(MinimalAPI)
class UCI_PopupHintable : public UInterface
{
	GENERATED_BODY()
};

class IDLESHOPINTHETOP_API ICI_PopupHintable
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Show();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Hide();
};
