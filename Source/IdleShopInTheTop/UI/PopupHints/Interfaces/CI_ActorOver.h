// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CI_ActorOver.generated.h"

UINTERFACE(MinimalAPI, meta = (Blueprintable))
class UCI_ActorOver : public UInterface
{
	GENERATED_BODY()
};

/**
 * ��������� ��� ���������� ������� ��������� ���� ��� �������������
 */
class IDLESHOPINTHETOP_API ICI_ActorOver
{
	GENERATED_BODY()

	public:
	/**
	* ��������� � ������� AActor OnBeginCursorOver
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void BeginCursorOver(UPrimitiveComponent* TouchedComponent);
	/**
	* ��������� � ������� AActor OnEndCursorOver
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndCursorOver(UPrimitiveComponent* TouchedComponent);
	/**
	* ��������� � ������� AActor OnInputTouchEnter
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void BeginTouchOver(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent);
	/**
	* ��������� � ������� AActor OnInputTouchLeave
	*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EndTouchOver(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent);
};
