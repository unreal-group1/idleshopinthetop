// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "CW_BasePopupHint.h"
#include "CW_PopupHintWithButton.generated.h"

/**
 * 
 */
UCLASS()
class IDLESHOPINTHETOP_API UCW_PopupHintWithButton : public UCW_BasePopupHint
{
	GENERATED_BODY()
	
	public:
	UPROPERTY(meta = (BindWidget))
	class UButton* B_Ok;
	UPROPERTY(meta = (BindWidget))
	class UButton* B_No;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	AC_Shelf* Shelf;

	virtual void Init() override;
	UFUNCTION()
	void OkClick();
	UFUNCTION()
	void NoClick();
	UFUNCTION()
	void UpdateHintText(AC_Shelf* ShelfUpdated, int32 MaxCount);
};
