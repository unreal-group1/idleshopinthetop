// SB Pavlov P.A.

#include "CW_PopupHint.h"
#include "C_Product.h"
#include "CI_InfoInHint.h"

#include "Components/TextBlock.h"

void UCW_PopupHint::Init()
{
    Super::Init();

    if (Product)
    {
        if (Product->StaticMeshComponent)
        {
            Product->StaticMeshComponent->OnBeginCursorOver.AddDynamic(this, &UCW_BasePopupHint::BeginCursorOver_Implementation);
            Product->StaticMeshComponent->OnEndCursorOver.AddDynamic(this, &UCW_BasePopupHint::EndCursorOver_Implementation);
            Product->StaticMeshComponent->OnInputTouchBegin.AddDynamic(this, &UCW_BasePopupHint::BeginTouchOver_Implementation);
            Product->StaticMeshComponent->OnInputTouchLeave.AddDynamic(this, &UCW_BasePopupHint::EndTouchOver_Implementation);
        }

        ICI_InfoInHint* InfoInHint = Cast<ICI_InfoInHint>(Product);
        if (InfoInHint)
        {
            if (TB_TextName)
            {
                TB_TextName->SetAutoWrapText(true);
                TB_TextName->SetText(InfoInHint->Execute_GetNameForHint(Product));
            }
            if (TB_TextHint)
            {
                TB_TextHint->SetAutoWrapText(true);
                TB_TextHint->SetText(InfoInHint->Execute_GetDescriptionForHint(Product));
            }
        }
    }

    bIsClickable = false;
}
