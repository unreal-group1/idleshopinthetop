// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "CW_BasePopupHint.h"
#include "CW_PopupHint.generated.h"

class AC_Product;

UCLASS()
class IDLESHOPINTHETOP_API UCW_PopupHint : public UCW_BasePopupHint
{
	GENERATED_BODY()

	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	AC_Product* Product;

	virtual void Init() override;
};
