// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "CI_ActorClick.h"
#include "CI_ActorOver.h"
#include "CI_PopupHintable.h"
#include "CW_BasePopupHint.generated.h"

/**
 * 
 */
UCLASS()
class IDLESHOPINTHETOP_API UCW_BasePopupHint : public UUserWidget, public ICI_PopupHintable, public ICI_ActorOver, public ICI_ActorClick
{
	GENERATED_BODY()

	public:
	UPROPERTY(meta = (BindWidget, MultiLine = true))
	class UTextBlock* TB_TextName;
	UPROPERTY(meta = (BindWidget, MultiLine = true))
	class UTextBlock* TB_TextHint;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	bool bIsClickable = false;

	UFUNCTION()
	virtual bool Initialize() override;
	UFUNCTION()
	virtual void Init();
	UFUNCTION()
	virtual void Show_Implementation() override;
	UFUNCTION()
	void Hide_Implementation() override;
	UFUNCTION()
	virtual void BeginCursorOver_Implementation(UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	virtual void EndCursorOver_Implementation(UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	virtual void BeginTouchOver_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	virtual void EndTouchOver_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	virtual void Clicked_Implementation(UPrimitiveComponent* TouchedComponent, 
		FKey ButtonPressed) override;
	UFUNCTION()
	virtual void Released_Implementation(UPrimitiveComponent* TouchedComponent, 
		FKey ButtonPressed) override;
	UFUNCTION()
	virtual void InputTouchBegin_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;
	UFUNCTION()
	virtual void InputTouchEnd_Implementation(ETouchIndex::Type FingerIndex,
		UPrimitiveComponent* TouchedComponent) override;

	//Popup Hint
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	float DelayHidePopupHint = 2.0f;
	private:
	void StartHideTimer();
	FTimerHandle PopupHintTimer;
};
