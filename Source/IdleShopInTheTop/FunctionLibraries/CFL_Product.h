// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"

#include "C_Product.h"
#include "CFL_Product.generated.h"

USTRUCT(BlueprintType)
struct FProduct
{
	GENERATED_BODY()
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AC_Product> ProductClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CurrentCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CountAddUpgradeMaxCount;
};

USTRUCT(BlueprintType)
struct FProductForSell
{
	GENERATED_BODY()
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AC_Product> ProductClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Cost;
};

UCLASS()
class IDLESHOPINTHETOP_API UCFL_Product : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
