// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "IdleShopInTheTopGameMode.generated.h"

UCLASS(minimalapi)
class AIdleShopInTheTopGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AIdleShopInTheTopGameMode();
};



