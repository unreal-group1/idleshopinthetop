// SB Pavlov P.A.

#include "C_Shelf.h"
#include "C_Product.h"
#include "CI_InteractableWithWallet.h"
#include "CI_InteractableWithBackpack.h"
#include "C_WalletComponent.h"
#include "C_BackpackComponent.h"
#include "C_PopupHintComponent.h"
#include "CI_Interactable.h"

#include "Math/UnrealMathUtility.h"
#include "Components/SphereComponent.h"
#include "Engine/StaticMesh.h"

AC_Shelf::AC_Shelf()
{
	PrimaryActorTick.bCanEverTick = false;

    if (!StaticMeshComponent)
    {
        StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));

    }
    if (StaticMeshComponent)
    {
        RootComponent = StaticMeshComponent;
    }

    if (!ProductsGetsCollider)
    {
        ProductsGetsCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
    }

    if (ProductsGetsCollider)
    {
        ProductsGetsCollider->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
        ProductsGetsCollider->SetCollisionProfileName(TEXT("Pawn"));
        ProductsGetsCollider->SetVisibility(true);

        ProductsGetsCollider->OnComponentBeginOverlap.AddDynamic(this, &AC_Shelf::BeginOverlap);
        ProductsGetsCollider->OnComponentEndOverlap.AddDynamic(this, &AC_Shelf::EndOverlap);
    }

    if (!PopupHintComponent)
    {
        PopupHintComponent = CreateDefaultSubobject<UC_PopupHintComponent>(TEXT("PopupHintComponent"));
    }

    if (PopupHintComponent)
    {
        PopupHintComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
    }
}

TArray<class AC_Product*> AC_Shelf::GetAllProductsInShelf()
{
    return ProductsInShelf;
}

void AC_Shelf::SpawnProducts(int32 ConutSpawnProduts)
{
    auto World = GetWorld();
    if (!World)
    {
        return;
    }

    AC_Product* CurrentProduct;
    for (int32 i = 0; i < ConutSpawnProduts; i++)
    {
        FTransform TransformNewProduct = GetTransform();
        FVector LocationNewProduct = TransformNewProduct.GetLocation();
        LocationNewProduct.X = LocationNewProduct.X + FMath::RandRange(-10.0, 10.0);
        LocationNewProduct.Y = LocationNewProduct.Y + FMath::RandRange(-10.0, 10.0);
        LocationNewProduct.Z = LocationNewProduct.Z + FMath::RandRange(0.0, 20.0);
        TransformNewProduct.SetLocation(LocationNewProduct);
        CurrentProduct = World->SpawnActor<AC_Product>(ProductClass, TransformNewProduct);
        ProductsInShelf.Add(CurrentProduct);
    }
}

void AC_Shelf::BeginOverlap(
    UPrimitiveComponent* OverlappedComponent, 
    AActor* OtherActor,
    UPrimitiveComponent* OtherComp, 
    int32 OtherBodyIndex, 
    bool bFromSweep, 
    const FHitResult& SweepResult)
{
    if (Cast<ICI_InteractableWithWallet>(OtherActor)
        && Cast<ICI_InteractableWithBackpack>(OtherActor))
    {
        OverlappedActor = OtherActor;

        if (GetWorldTimerManager().IsTimerPaused(GoToTargetProductFromShelfTimer))
        {
            GetWorldTimerManager().UnPauseTimer(GoToTargetProductFromShelfTimer);
        }
        else
        {
            GetWorldTimerManager().SetTimer(GoToTargetProductFromShelfTimer,
                this,
                &AC_Shelf::StartMovingProduct,
                DelayProdutGoToTarget,
                true);
        }
    }
}

void AC_Shelf::EndOverlap(
    UPrimitiveComponent* OverlappedComponent, 
    AActor* OtherActor, 
    UPrimitiveComponent* OtherComp, 
    int32 OtherBodyIndex)
{
    if (Cast<ICI_InteractableWithWallet>(OtherActor)
        && Cast<ICI_InteractableWithBackpack>(OtherActor))
    {
        OverlappedActor = nullptr;
        GetWorldTimerManager().PauseTimer(GoToTargetProductFromShelfTimer);
    }
}

void AC_Shelf::BeginPlay()
{
	Super::BeginPlay();

    SpawnProducts(MaxCountProduts);
}

void AC_Shelf::StartMovingProduct()
{
    if (OverlappedActor)
    {
        int32 CountProducts = ProductsInShelf.Num() - 1;
        if (CountProducts > 0)
        {
            auto ActorWithBackpack = Cast<ICI_InteractableWithBackpack>(OverlappedActor);

            auto Backpack = ActorWithBackpack->Execute_GetBackpackComponent(OverlappedActor);
            auto CurrentCountProduct = Backpack->GetCurrentCount(ProductClass);
            auto MaxCountProduct = Backpack->GetMaxCount(ProductClass);

            if (ProductsInShelf[CountProducts]
                && CurrentCountProduct < MaxCountProduct)
            {
                ProductsInShelf[CountProducts]->Execute_GoToTarget(
                    ProductsInShelf[CountProducts],
                    OverlappedActor,
                    ActorWithBackpack->Execute_GetBackpackLocation(OverlappedActor));

                ProductsInShelf.RemoveAt(CountProducts);
            }
        }
        else
        {
            SpawnProducts(MaxCountProduts);
            StartMovingProduct();
        }
    }
}

FText AC_Shelf::GetNameForHint_Implementation()
{
    return Name;
}

FText AC_Shelf::GetDescriptionForHint_Implementation()
{
    FString TextDescription = FString::Printf(TEXT("%s \n %s %i"), *Description.ToString(), *UpgradeText.ToString(), CurrentCostUpgrade);
    return FText::FromString(TextDescription);
}

void AC_Shelf::Upgrade_Implementation(AActor* UpdatingActor, int32 Cost)
{
    auto ActorWithWallet = Cast<ICI_InteractableWithWallet>(UpdatingActor);
    auto ActorWithBackpack = Cast<ICI_InteractableWithBackpack>(UpdatingActor);

    if (ActorWithWallet
        && ActorWithBackpack)
    {
        bool ResultBuy = false;
        auto WalletComponent = ActorWithWallet->Execute_GetWalletComponent(UpdatingActor);
        if (WalletComponent)
        {
            ResultBuy = WalletComponent->Buy(Cost);
        }

        if (ResultBuy)
        {
            CurrentCostUpgrade = CurrentCostUpgrade * CoefficientUpdateCostUpgrade;

            auto Backpack = ActorWithBackpack->Execute_GetBackpackComponent(UpdatingActor);
            if (Backpack)
            {
                if (ProductClass)
                {
                    Backpack->UpgradeMaxCount(ProductClass);
                }
            }

            OnChangeCurrentCostUpgrade.Broadcast(this, CurrentCostUpgrade);
        }
    }
}
