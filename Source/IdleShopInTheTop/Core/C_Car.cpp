// SB Pavlov P.A.

#include "C_Car.h"
#include "C_Product.h"
#include "CI_InteractableWithWallet.h"
#include "CI_InteractableWithBackpack.h"
#include "C_WalletComponent.h"
#include "C_BackpackComponent.h"
#include "C_IdleShopInTheTopInstance.h"

#include "Components/SphereComponent.h"

AC_Car::AC_Car()
{
	PrimaryActorTick.bCanEverTick = false;

	if (!StaticMeshComponent)
	{
		StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
		RootComponent = StaticMeshComponent;
		StaticMeshComponent->SetCollisionProfileName("BlockAll");
	}

    if (!ProductsGetsCollider)
    {
        ProductsGetsCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
        ProductsGetsCollider->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
        ProductsGetsCollider->SetCollisionProfileName(TEXT("Pawn"));
        ProductsGetsCollider->SetVisibility(true);

        ProductsGetsCollider->OnComponentBeginOverlap.AddDynamic(this, &AC_Car::BeginOverlap);
        ProductsGetsCollider->OnComponentEndOverlap.AddDynamic(this, &AC_Car::EndOverlap);
    }
}

void AC_Car::BeginOverlap(UPrimitiveComponent* OverlappedComponent, 
    AActor* OtherActor, 
    UPrimitiveComponent* OtherComp, 
    int32 OtherBodyIndex, 
    bool bFromSweep, 
    const FHitResult& SweepResult)
{
    if (Cast<ICI_InteractableWithBackpack>(OtherActor))
    {
        OverlappedActor = OtherActor;

        if (GetWorldTimerManager().IsTimerPaused(GoToTargetProductToCarTimer))
        {
            GetWorldTimerManager().UnPauseTimer(GoToTargetProductToCarTimer);
        }
        else
        {
            GetWorldTimerManager().SetTimer(GoToTargetProductToCarTimer,
                this,
                &AC_Car::StartMovingProduct,
                DelayProdutGoToTarget,
                true);
        }
    }
}

void AC_Car::EndOverlap(UPrimitiveComponent* OverlappedComponent, 
    AActor* OtherActor, 
    UPrimitiveComponent* OtherComp, 
    int32 OtherBodyIndex)
{
    if (Cast<ICI_InteractableWithBackpack>(OtherActor))
    {
        OverlappedActor = nullptr;
        GetWorldTimerManager().PauseTimer(GoToTargetProductToCarTimer);
    }
}

void AC_Car::BeginPlay()
{
	Super::BeginPlay();
}

void AC_Car::Sell_Implementation(AActor* SellingActor, int32 Cost)
{
    if (OverlappedActor)
    {
        bool ResultSell = false; 
        auto Backpack = Cast<ICI_InteractableWithBackpack>(OverlappedActor)->Execute_GetBackpackComponent(OverlappedActor);

        if (Backpack)
        {
            auto SellingProduct = Cast<AC_Product>(SellingActor);
            if (SellingProduct)
            {
                auto ResultDecrement = Backpack->DecrementCurrentCount(SellingProduct->GetClass());

                if (ResultDecrement)
                {
                    auto Wallet = Cast<ICI_InteractableWithWallet>(OverlappedActor)->Execute_GetWalletComponent(OverlappedActor);
                    if (Wallet)
                    {
                        ResultSell = Wallet->Sell(Cost);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }
    }
}

void AC_Car::StartMovingProduct()
{
    if (OverlappedActor)
    {
        auto Backpack = Cast<ICI_InteractableWithBackpack>(OverlappedActor)->Execute_GetBackpackComponent(OverlappedActor);
        //����� ������ � ��������� ������ � ���� ������
        auto Product = Backpack->FindProductInStock();
        if (Product)
        {
            int32 Cost = 0;

            for (auto& CurrentProduct : ProductsForSell)
            {
                if (Product == CurrentProduct.ProductClass)
                {
                    Cost = CurrentProduct.Cost;
                }
            }

            if (Cost > 0)
            { 
                if (GetWorld())
                {
                    FActorSpawnParameters SpawnParameters;
                    SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

                    auto Transform = OverlappedActor->GetTransform();
                    auto SpawnedActor = GetWorld()->SpawnActor(Product,
                        &Transform,
                        SpawnParameters);
                    auto SpawnedProduct = Cast<AC_Product>(SpawnedActor);
                    
                    FHitResult Hit;
                    SpawnedProduct->SetActorLocation(Transform.GetLocation(), false, &Hit, ETeleportType::TeleportPhysics);

                    if (SpawnedProduct)
                    {
                        Sell_Implementation(SpawnedProduct, Cost);

                        UE_LOG(LogTemp, Warning, TEXT("Car. SpawnedProductName: %s"), *SpawnedProduct->GetActorLocation().ToString());
                        UE_LOG(LogTemp, Warning, TEXT("Car. SpawnedProductName: %s"), *SpawnedProduct->GetName());

                        SpawnedProduct->Execute_GoToTarget(
                            SpawnedProduct,
                            this,
                            GetActorLocation());
                    }
                }
            }
        }
    }
}
