// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "CFL_Product.h"
#include "CI_Selled.h"
#include "C_Car.generated.h"

class AC_Product;
struct F_Product;

UCLASS()
class IDLESHOPINTHETOP_API AC_Car : public AActor, public ICI_Selled
{
	GENERATED_BODY()
	
public:	
	AC_Car();
	//CollisionProductsGets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Car)
	class USphereComponent* ProductsGetsCollider;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Car)
	TArray<FProductForSell> ProductsForSell;

	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Car)
	class UStaticMeshComponent* StaticMeshComponent;

protected:
	virtual void BeginPlay() override;

	//ICI_Selled
	public:
	void Sell_Implementation(AActor* SellingActor, int32 Cost) override;
	//GoToTarget Selled Product
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Car)
	float DelayProdutGoToTarget = 1.0f;
	private:
	FTimerHandle GoToTargetProductToCarTimer;

	AActor* OverlappedActor;

	void StartMovingProduct();
};
