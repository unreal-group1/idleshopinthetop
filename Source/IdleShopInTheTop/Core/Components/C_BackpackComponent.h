// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "C_BackpackComponent.generated.h"

class AC_Product;
struct FProduct;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeCurrentCount, TSubclassOf<AC_Product>, ProductClass, int32, CurrentCount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeMaxCount, TSubclassOf<AC_Product>, ProductClass, int32, MaxCount);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class IDLESHOPINTHETOP_API UC_BackpackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UC_BackpackComponent();

	FOnChangeCurrentCount OnChangeCurrentCount;
	FOnChangeMaxCount OnChangeMaxCount;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Backpack)
	FVector BackpackLocation;

	int32 GetCurrentCount(TSubclassOf<AC_Product> TargetProductClass);
	int32 GetMaxCount(TSubclassOf<AC_Product> TargetProductClass);

	TSubclassOf<AC_Product> FindProductInStock();
	FProduct FindProduct(TSubclassOf<AC_Product> TargetProductClass);

	FVector GetBackpackLocation();

	bool IncrementCurrentCount(TSubclassOf<AC_Product> TargetProductClass);
	bool DecrementCurrentCount(TSubclassOf<AC_Product> TargetProductClass);
	bool UpgradeMaxCount(TSubclassOf<AC_Product> TargetProductClass);
	UFUNCTION()
	bool IsEmpty();
	UFUNCTION()
	bool IsFull();

protected:
	virtual void BeginPlay() override;

	private:
	TArray<FProduct> Products;
};
