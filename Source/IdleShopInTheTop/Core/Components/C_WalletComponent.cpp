// SB Pavlov P.A.

#include "C_WalletComponent.h"

UC_WalletComponent::UC_WalletComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


int32 UC_WalletComponent::GetCurrentCountMoney()
{
	return CurrentMoney;
}

bool UC_WalletComponent::Buy(int32 Cost)
{
	if (CurrentMoney < Cost)
	{
		return false;
	}
	else
	{
		CurrentMoney -= Cost;
		OnChangeCurrentMoney.Broadcast(CurrentMoney);
		return true;
	}
}

bool UC_WalletComponent::Sell(int32 Cost)
{
	CurrentMoney += Cost;
	OnChangeCurrentMoney.Broadcast(CurrentMoney);
	return true;
}

void UC_WalletComponent::BeginPlay()
{
	Super::BeginPlay();
}

