// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "C_WalletComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeCurrentMoney, int32, CurrentMoney);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class IDLESHOPINTHETOP_API UC_WalletComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UC_WalletComponent();

	FOnChangeCurrentMoney OnChangeCurrentMoney;

	int32 GetCurrentCountMoney();
	bool Buy(int32 Cost);
	bool Sell(int32 Cost);

protected:
	virtual void BeginPlay() override;

public:	
	private:
	int32 CurrentMoney = 0;
};
