// SB Pavlov P.A.

#include "C_BackpackComponent.h"
#include "C_Product.h"
#include "CFL_Product.h"
#include "C_IdleShopInTheTopInstance.h"

#include "Containers/Set.h"

UC_BackpackComponent::UC_BackpackComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	auto World = GetWorld();
	if (World)
	{
		auto Instance = Cast<UC_IdleShopInTheTopInstance>(World->GetGameInstance());
		if (Instance)
		{
			for (auto ProductClass : Instance->DefaultProductClases)
			{
				FProduct NewProduct;
				NewProduct.ProductClass = ProductClass;
				NewProduct.CurrentCount = 0;
				NewProduct.MaxCount = 10;
				NewProduct.CountAddUpgradeMaxCount = 10;
				Products.Add(NewProduct);
			}
		}
	}
}

int32 UC_BackpackComponent::GetCurrentCount(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto CurrentProduct : Products)
	{
		if (CurrentProduct.ProductClass == TargetProductClass)
		{
			return CurrentProduct.CurrentCount;
		}
	}
	
	return 0;
}

bool UC_BackpackComponent::IncrementCurrentCount(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto& CurrentProduct : Products)
	{
		if (CurrentProduct.ProductClass == TargetProductClass)
		{
			if (CurrentProduct.CurrentCount < CurrentProduct.MaxCount)
			{
				CurrentProduct.CurrentCount++;
				OnChangeCurrentCount.Broadcast(CurrentProduct.ProductClass, CurrentProduct.CurrentCount);
				return true;
			}
		}
	}

	return false;
}

bool UC_BackpackComponent::DecrementCurrentCount(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto& CurrentProduct : Products)
	{
		if (CurrentProduct.ProductClass == TargetProductClass)
		{
			if (CurrentProduct.CurrentCount > 0)
			{
				CurrentProduct.CurrentCount--;
				OnChangeCurrentCount.Broadcast(CurrentProduct.ProductClass, CurrentProduct.CurrentCount);
				return true;
			}
		}
	}

	return false;
}

int32 UC_BackpackComponent::GetMaxCount(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto CurrentProduct : Products)
	{
		if (CurrentProduct.ProductClass == TargetProductClass)
		{
			return CurrentProduct.MaxCount;
		}
	}

	return 0;
}

TSubclassOf<AC_Product> UC_BackpackComponent::FindProductInStock()
{
	for (auto CurrentProduct : Products)
	{
		if (CurrentProduct.CurrentCount > 0)
		{
			return CurrentProduct.ProductClass;
		}
	}

	return nullptr;
}

FProduct UC_BackpackComponent::FindProduct(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto CurrentProduct : Products)
	{
		if (TargetProductClass == CurrentProduct.ProductClass)
		{
			return CurrentProduct;
		}
	}

	return FProduct();
}

FVector UC_BackpackComponent::GetBackpackLocation()
{
	return BackpackLocation;
}

bool UC_BackpackComponent::UpgradeMaxCount(TSubclassOf<AC_Product> TargetProductClass)
{
	for (auto& CurrentProduct : Products)
	{
		if (CurrentProduct.ProductClass == TargetProductClass)
		{
			CurrentProduct.MaxCount += CurrentProduct.CountAddUpgradeMaxCount;
			OnChangeMaxCount.Broadcast(CurrentProduct.ProductClass, CurrentProduct.MaxCount);
			return true;
		}
	}

	return false;
}

bool UC_BackpackComponent::IsEmpty()
{
	for (auto Product : Products)
	{
		if (Product.CurrentCount != 0)
		{
			return false;
		}
	}

	return true;
}

bool UC_BackpackComponent::IsFull()
{
	for (auto Product : Products)
	{
		if (Product.CurrentCount != Product.MaxCount)
		{
			return false;
		}
	}

	return true;
}

void UC_BackpackComponent::BeginPlay()
{
	Super::BeginPlay();
}
