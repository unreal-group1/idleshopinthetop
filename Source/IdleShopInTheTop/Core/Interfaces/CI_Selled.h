// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CI_Selled.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UCI_Selled : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IDLESHOPINTHETOP_API ICI_Selled
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Sell(AActor* SellingActor, int32 Cost);
};
