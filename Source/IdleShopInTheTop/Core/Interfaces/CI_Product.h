// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "CI_ActorOver.h"
#include "CI_Product.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UCI_Product : public UInterface
{
	GENERATED_BODY()
};

class IDLESHOPINTHETOP_API ICI_Product
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void GoToTarget(AActor* Target, FVector BackpackLocation);
};
