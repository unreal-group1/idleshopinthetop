// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CI_Upgraded.generated.h"

UINTERFACE(MinimalAPI, Blueprintable)
class UCI_Upgraded : public UInterface
{
	GENERATED_BODY()
};

class IDLESHOPINTHETOP_API ICI_Upgraded
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Upgrade(AActor* UpdatingActor, int32 Cost);
};
