// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "C_WalletComponent.h"
#include "CI_InteractableWithWallet.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCI_InteractableWithWallet : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IDLESHOPINTHETOP_API ICI_InteractableWithWallet
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	UC_WalletComponent* GetWalletComponent();
};
