// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"

#include "C_BackpackComponent.h"
#include "CI_InteractableWithBackpack.generated.h"

UINTERFACE(MinimalAPI)
class UCI_InteractableWithBackpack : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class IDLESHOPINTHETOP_API ICI_InteractableWithBackpack
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	UC_BackpackComponent* GetBackpackComponent();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector GetBackpackLocation();
};
