// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "CI_Upgraded.h"
#include "CI_InfoInHint.h"
#include "C_Shelf.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnChangeCurrentCostUpgrade, AC_Shelf*, Shelf, int32, Cost);

UCLASS()
class IDLESHOPINTHETOP_API AC_Shelf : public AActor, public ICI_InfoInHint, public ICI_Upgraded
{
	GENERATED_BODY()
	
public:
	AC_Shelf();

	FOnChangeCurrentCostUpgrade OnChangeCurrentCostUpgrade;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shelf)
	class USphereComponent* ProductsGetsCollider;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shelf)
	class UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shelf)
	TSubclassOf<class AC_Product> ProductClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shelf)
	int32 MaxCountProduts = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shelf)
	float DelayProdutGoToTarget = 1.0f;
	
	UFUNCTION()
	TArray<class AC_Product*> GetAllProductsInShelf();

	UFUNCTION()
	void SpawnProducts(int32 ConutSpawnProduts);

	UFUNCTION()
	void BeginOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex, 
		bool bFromSweep, 
		const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlap(
		UPrimitiveComponent* OverlappedComponent, 
		AActor* OtherActor, 
		UPrimitiveComponent* OtherComp, 
		int32 OtherBodyIndex);
protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleAnywhere)
	TArray<class AC_Product*> ProductsInShelf;

	FTimerHandle GoToTargetProductFromShelfTimer;

	AActor* OverlappedActor;

	void StartMovingProduct();

	//ICI_InfoInHint
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	FText Name = FText::FromString("Default");
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	FText Description = FText::FromString("Default");
	FText GetNameForHint_Implementation() override;
	FText GetDescriptionForHint_Implementation() override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	FText UpgradeText = FText::FromString("Стоимость обновления: ");
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
	class UC_PopupHintComponent* PopupHintComponent;

	//ICI_Upgraded
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	int CurrentCostUpgrade = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	int CoefficientUpdateCostUpgrade = 2;
	void Upgrade_Implementation(AActor* UpdatingActor, int32 Cost) override;
};
