// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "CI_InfoInHint.h"
#include "CI_Product.h"
#include "C_Product.generated.h"

UCLASS()
class IDLESHOPINTHETOP_API AC_Product : public AActor, public ICI_Product, public ICI_InfoInHint
{
	GENERATED_BODY()
	
public:	
	AC_Product();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	class UStaticMeshComponent* StaticMeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	class UProjectileMovementComponent* ProjectileMovementComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
	class UC_PopupHintComponent* PopupHintComponent;
	//ICI_Product
	void GoToTarget_Implementation(AActor* Target, FVector BackpackLocation) override;
	//ICI_InfoInHint
	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	FText Name = FText::FromString("Default");
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Product)
	FText Description = FText::FromString("Default");
	FText GetNameForHint_Implementation() override;
	FText GetDescriptionForHint_Implementation() override;

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	private:
	float MoveSpeed = 400.0f;
	FVector CurrentVelocity = FVector::ZeroVector;
	UFUNCTION()
	void ProductHit(const FHitResult& ImpactResult);
};
