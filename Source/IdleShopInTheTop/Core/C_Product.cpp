// SB Pavlov P.A.

#include "C_Product.h"
#include "CI_Selled.h"
#include "CI_InteractableWithBackpack.h"
#include "C_BackpackComponent.h"
#include "C_GameHUD.h"
#include "C_PopupHintComponent.h"

#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

AC_Product::AC_Product()
{
	PrimaryActorTick.bCanEverTick = false;

	if (!StaticMeshComponent)
	{
		StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	}
	if (StaticMeshComponent)
	{
		RootComponent = StaticMeshComponent;
		StaticMeshComponent->SetCollisionProfileName("WorldDynamic");
		StaticMeshComponent->SetSimulatePhysics(true);

		StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Block);
		StaticMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	}

	if (!ProjectileMovementComponent)
	{
		ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	}
	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->Velocity = FVector(0.0f, 0.0f, 0.0f);
		ProjectileMovementComponent->bRotationFollowsVelocity = true;
		ProjectileMovementComponent->bInterpRotation = true;
		ProjectileMovementComponent->InterpRotationTime = 0.1f;
		ProjectileMovementComponent->bInterpMovement = true;
		ProjectileMovementComponent->InitialSpeed = MoveSpeed;
		ProjectileMovementComponent->MaxSpeed = MoveSpeed;
		ProjectileMovementComponent->bIsHomingProjectile = true;
		ProjectileMovementComponent->HomingAccelerationMagnitude = MoveSpeed;
		ProjectileMovementComponent->ProjectileGravityScale = 0.0f;
		ProjectileMovementComponent->bSimulationEnabled = false;
		ProjectileMovementComponent->bSweepCollision = true;
		ProjectileMovementComponent->bAutoActivate = false;

		ProjectileMovementComponent->OnProjectileStop.AddDynamic(this, &AC_Product::ProductHit);
	}

	if (!PopupHintComponent)
	{
		PopupHintComponent = CreateDefaultSubobject<UC_PopupHintComponent>(TEXT("PopupHintComponent"));
	}
	if (PopupHintComponent)
	{
		PopupHintComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	}
}

void AC_Product::GoToTarget_Implementation(AActor* Target, FVector BackpackLocation)
{
	FRotator CurrentRotator = FRotator();

	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->HomingTargetComponent = Target->GetRootComponent();

		ProjectileMovementComponent->bSimulationEnabled = true;
	}

	if (StaticMeshComponent)
	{
		StaticMeshComponent->SetSimulatePhysics(false);
		if (Cast<ICI_Selled>(Target))
		{
			StaticMeshComponent->SetCollisionProfileName("ProductForSale");
		}
		else
		{
			StaticMeshComponent->SetCollisionProfileName("Product");
		}
	}

	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->Activate();
	}
}

FText AC_Product::GetNameForHint_Implementation()
{
	return Name;
}

FText AC_Product::GetDescriptionForHint_Implementation()
{
	return Description;
}

void AC_Product::BeginPlay()
{
	Super::BeginPlay();
}

void AC_Product::Tick(float DeltaSeconds)
{
	if (ProjectileMovementComponent)
	{
		ProjectileMovementComponent->ComputeHomingAcceleration(CurrentVelocity, DeltaSeconds);
	}
}

void AC_Product::ProductHit(const FHitResult& ImpactResult)
{
	UE_LOG(LogTemp, Warning, TEXT("%s hit: %s"), *this->GetName(), *ImpactResult.GetActor()->GetName());
	if (auto ActorWithBackpack = Cast<ICI_InteractableWithBackpack>(ImpactResult.GetActor()))
	{
		auto Backpack = ActorWithBackpack->Execute_GetBackpackComponent(ImpactResult.GetActor());
		Backpack->IncrementCurrentCount(this->GetClass());
		Destroy();
	}
	else if (auto Selled = Cast<ICI_Selled>(ImpactResult.GetActor()))
	{
		Destroy();
	}
}
