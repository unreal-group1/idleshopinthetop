// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "CI_InteractableWithBackpack.h"
#include "CI_InteractableWithWallet.h"
#include "CI_Interactable.h"
#include "IdleShopInTheTopCharacter.generated.h"

UCLASS(Blueprintable)
class AIdleShopInTheTopCharacter : public ACharacter, 
	public ICI_Interactable, 
	public ICI_InteractableWithBackpack, 
	public ICI_InteractableWithWallet
{
	GENERATED_BODY()

	public:
	AIdleShopInTheTopCharacter();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Backpack)
	class UC_BackpackComponent* BackpackComponent;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Backpack)
	class UC_WalletComponent* WalletComponent;

	UC_BackpackComponent* GetBackpackComponent_Implementation() override;
	UC_WalletComponent* GetWalletComponent_Implementation() override;
	FVector GetBackpackLocation_Implementation() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	//������������ ���������� �� ��, ����� ������������ �����-�� ����� �� ��������� ����������
	public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI)
	bool bAIControlled = false;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI)
	float MaxMoveSpeed = 500.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI)
	float MaxAIMoveSpeed = 200.0f;

	UFUNCTION()
	void StartAIControl();
	UFUNCTION()
	void StopAIControl();
	private:
	TArray<class AC_Shelf*> Shelfs;
	TArray<class AC_Car*> Cars;
	UWorld* CurrentWorld;
	class UC_IdleShopInTheTopInstance* Instance;
	class AIdleShopInTheTopPlayerController* Controller;

	FVector DefaultTargetLocation = FVector(DOUBLE_BIG_NUMBER);
	FVector LastTargetLocation = DefaultTargetLocation;
	float ApproachDistancesPoint = 200.0f;

	bool GetActorsForGoTo();
	bool CollectAllProducts();
	bool SellAllProducts();
	/// ������ � ���� ��� ����� ������� ��� �������
	private:
	bool GoToTarget(FVector TargetLocation);
	/// ������� �������� � ���������� �������� ��
	public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = AI)
	float DefaultTimeToActivateAIControl = 2.0f;
	private:
	bool bIsGetAllProducts = false;

	FTimerHandle AIControlTryTimer;
};

