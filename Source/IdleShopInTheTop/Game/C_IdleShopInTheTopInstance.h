// SB Pavlov P.A.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "C_Car.h"
#include "C_Shelf.h"
#include "C_Product.h"
#include "C_IdleShopInTheTopInstance.generated.h"

/**
 * 
 */
UCLASS()
class IDLESHOPINTHETOP_API UC_IdleShopInTheTopInstance : public UGameInstance
{
	GENERATED_BODY()

	public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Products)
	TArray<TSubclassOf<AC_Product>> DefaultProductClases;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shelfs)
	TArray<TSubclassOf<AC_Shelf>> DefaultShelfClases;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shelfs)
	TArray<TSubclassOf<AC_Car>> DefaultCarClases;
};
