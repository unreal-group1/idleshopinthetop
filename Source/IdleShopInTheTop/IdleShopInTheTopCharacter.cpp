// Copyright Epic Games, Inc. All Rights Reserved.

#include "IdleShopInTheTopCharacter.h"
#include "C_Shelf.h"
#include "C_Car.h"
#include "C_IdleShopInTheTopInstance.h"
#include "IdleShopInTheTopPlayerController.h"
#include "C_BackpackComponent.h"
#include "C_WalletComponent.h"

#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraFunctionLibrary.h"

AIdleShopInTheTopCharacter::AIdleShopInTheTopCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	BackpackComponent = CreateDefaultSubobject<UC_BackpackComponent>(TEXT("BackpackComponent"));
	WalletComponent = CreateDefaultSubobject<UC_WalletComponent>(TEXT("WalletComponent"));
}

void AIdleShopInTheTopCharacter::BeginPlay()
{
	Super::BeginPlay();

	Controller = Cast<AIdleShopInTheTopPlayerController>(GetController());
}

void AIdleShopInTheTopCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

UC_BackpackComponent* AIdleShopInTheTopCharacter::GetBackpackComponent_Implementation()
{
	return BackpackComponent;
}

UC_WalletComponent* AIdleShopInTheTopCharacter::GetWalletComponent_Implementation()
{
	return WalletComponent;
}

FVector AIdleShopInTheTopCharacter::GetBackpackLocation_Implementation()
{
	return BackpackComponent->GetBackpackLocation();
}

void AIdleShopInTheTopCharacter::StartAIControl()
{
	bAIControlled = true;
	UE_LOG(LogTemp, Warning, TEXT("StartAIControl"));

	if (!GetWorldTimerManager().IsTimerActive(AIControlTryTimer))
	{
		GetWorldTimerManager().SetTimer(AIControlTryTimer,
			this,
			&AIdleShopInTheTopCharacter::StartAIControl,
			DefaultTimeToActivateAIControl,
			true);
	}

	auto bGoToIsGet = GetActorsForGoTo();
	if (bGoToIsGet)
	{
		if (bIsGetAllProducts)
		{
			SellAllProducts();
		}
		else
		{
			CollectAllProducts();
		}
	}

	GetCharacterMovement()->MaxWalkSpeed = MaxAIMoveSpeed;
}
void AIdleShopInTheTopCharacter::StopAIControl()
{
	UE_LOG(LogTemp, Warning, TEXT("StopAIControl"));
	GetWorldTimerManager().ClearTimer(AIControlTryTimer);
	
	GetCharacterMovement()->MaxWalkSpeed = MaxMoveSpeed;
}

/**
* �������, � ������� ����� �����
*/
bool AIdleShopInTheTopCharacter::GetActorsForGoTo()
{
	UE_LOG(LogTemp, Warning, TEXT("GetActorsForGoTo"));
	if (!CurrentWorld)
	{
		CurrentWorld = GetWorld();
	}

	if (!CurrentWorld)
	{
		return false;
	}

	if (Shelfs.Num() <= 0)
	{
		if (!Instance)
		{
			Instance = Cast<UC_IdleShopInTheTopInstance>(GetGameInstance());
		}

		if (!Instance)
		{
			return false;
		}

		if (Instance
			&& Shelfs.Num() <= 0)
		{
			TArray<AActor*> ActorsShelf;
			for (auto ShelfClass : Instance->DefaultShelfClases)
			{
				UGameplayStatics::GetAllActorsOfClass(CurrentWorld, ShelfClass, ActorsShelf);

				for (auto ActorShelf : ActorsShelf)
				{
					Shelfs.Add(Cast<AC_Shelf>(ActorShelf));
				}
			}
		}
	}

	if (Cars.Num() <= 0)
	{
		if (Instance)
		{
			TArray<AActor*> ActorsCars;
			for (auto CarClass : Instance->DefaultCarClases)
			{
				UGameplayStatics::GetAllActorsOfClass(CurrentWorld, CarClass, ActorsCars);

				for (auto ActorCar : ActorsCars)
				{
					Cars.Add(Cast<AC_Car>(ActorCar));
				}
			}
		}
	}

	return true;
}
/**
* ������� ��� ��������
*/
bool AIdleShopInTheTopCharacter::CollectAllProducts()
{
	UE_LOG(LogTemp, Warning, TEXT("CollectAllProducts"));
	if (Shelfs.Num() > 0)
	{
		if (!BackpackComponent->IsFull())
		{
			//����� ���� ����� � ����������
			for (auto Shelf : Shelfs)
			{
				auto ProductsInBackpack = BackpackComponent->FindProduct(Shelf->ProductClass);
				if (ProductsInBackpack.ProductClass)
				{
					if (ProductsInBackpack.CurrentCount <= ProductsInBackpack.MaxCount)
					{
						LastTargetLocation = Shelf->GetActorLocation();
						GoToTarget(LastTargetLocation);
						return false;
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("This product is full"));
						continue;
					}
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("ProductClass not found"));
					continue;
				}
			}
		}
		else
		{
			bIsGetAllProducts = true;
			UE_LOG(LogTemp, Warning, TEXT("Backpack is full"));
		}

		return true;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Shelfs.Num() <= 0"));
		return false;
	}
}

bool AIdleShopInTheTopCharacter::SellAllProducts()
{
	UE_LOG(LogTemp, Warning, TEXT("SellAllProducts"));
	if (!Instance)
	{
		Instance = Cast<UC_IdleShopInTheTopInstance>(GetGameInstance());
	}

	if (!Instance)
	{
		return false;
	}
	//����� ���� ����� ��� �������
	if (Cars.Num() > 0)
	{
		if (!BackpackComponent->IsEmpty())
		{
			for (auto Car : Cars)
			{
				for (auto ProductClass : Instance->DefaultProductClases)
				{
					auto ProductsInBackpack = BackpackComponent->FindProduct(ProductClass);
					if (ProductsInBackpack.ProductClass)
					{
						if (ProductsInBackpack.CurrentCount != 0)
						{
							LastTargetLocation = Car->GetActorLocation();
							GoToTarget(LastTargetLocation);
							return false;
						}
						else
						{
							UE_LOG(LogTemp, Warning, TEXT("This product is full"));
							continue;
						}
					}
					else
					{
						UE_LOG(LogTemp, Warning, TEXT("ProductClass not found"));
						continue;
					}
				}
			}
		}
		else
		{
			bIsGetAllProducts = false;
		}
	}

	return false;
}

bool AIdleShopInTheTopCharacter::GoToTarget(FVector TargetLocation)
{
	UE_LOG(LogTemp, Warning, TEXT("GoToTarget"));
	if (FVector::Distance(TargetLocation, DefaultTargetLocation) > ApproachDistancesPoint)
	{
		if (FVector::Distance(TargetLocation, GetActorLocation()) > ApproachDistancesPoint)
		{
			if (Controller)
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(Controller,
					TargetLocation);
				UNiagaraFunctionLibrary::SpawnSystemAtLocation(Controller,
					Controller->FXCursor,
					TargetLocation,
					FRotator::ZeroRotator,
					FVector(1.0f, 1.0f, 1.0f),
					true,
					true,
					ENCPoolMethod::None,
					true);
				UE_LOG(LogTemp, Warning, TEXT("%s go to %s"), *this->GetName(), *TargetLocation.ToString());
			}

			return false;
		}
		else
		{
			return true;
		}
	}

	return false;
}