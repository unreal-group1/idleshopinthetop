// Copyright Epic Games, Inc. All Rights Reserved.

#include "IdleShopInTheTopPlayerController.h"
#include "IdleShopInTheTopCharacter.h"

#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "InputActionValue.h"
#include "EnhancedInputSubsystems.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

AIdleShopInTheTopPlayerController::AIdleShopInTheTopPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	FollowTime = 0.f;
	//For PopupHints
	bEnableMouseOverEvents = true;
	bEnableTouchOverEvents = true;
	//For Upgrade Widget
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
}

void AIdleShopInTheTopPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}

	if (!GetWorldTimerManager().IsTimerActive(AIControlActivateTimer))
	{
		GetWorldTimerManager().SetTimer(AIControlActivateTimer,
			this,
			&AIdleShopInTheTopPlayerController::StartAIControl,
			TimeToActivateAIControl,
			true);
	}
}

void AIdleShopInTheTopPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Started, this, &AIdleShopInTheTopPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AIdleShopInTheTopPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AIdleShopInTheTopPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AIdleShopInTheTopPlayerController::OnSetDestinationReleased);

		// Setup touch input events
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Started, this, &AIdleShopInTheTopPlayerController::OnInputStarted);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Triggered, this, &AIdleShopInTheTopPlayerController::OnTouchTriggered);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Completed, this, &AIdleShopInTheTopPlayerController::OnTouchReleased);
		EnhancedInputComponent->BindAction(SetDestinationTouchAction, ETriggerEvent::Canceled, this, &AIdleShopInTheTopPlayerController::OnTouchReleased);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void AIdleShopInTheTopPlayerController::OnInputStarted()
{
	StopMovement();

	UE_LOG(LogTemp, Warning, TEXT("OnInputStarted time %f"), TimeToActivateAIControl);

	if (!GetWorldTimerManager().IsTimerActive(AIControlActivateTimer))
	{
		GetWorldTimerManager().SetTimer(AIControlActivateTimer,
			this,
			&AIdleShopInTheTopPlayerController::StartAIControl,
			TimeToActivateAIControl,
			true);
	}
}

// Triggered every frame when the input is held down
void AIdleShopInTheTopPlayerController::OnSetDestinationTriggered()
{
	// We flag that the input is being pressed
	FollowTime += GetWorld()->GetDeltaSeconds();
	
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = false;
	if (bIsTouch)
	{
		bHitSuccessful = GetHitResultUnderFinger(ETouchIndex::Touch1, ECollisionChannel::ECC_Visibility, true, Hit);
	}
	else
	{
		bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);
	}

	// If we hit a surface, cache the location
	if (bHitSuccessful)
	{
		CachedDestination = Hit.Location;
	}
	
	// Move towards mouse pointer or touch
	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn != nullptr)
	{
		FVector WorldDirection = (CachedDestination - ControlledPawn->GetActorLocation()).GetSafeNormal();
		ControlledPawn->AddMovementInput(WorldDirection, 1.0, false);
	}

	StopAIControl();
}

void AIdleShopInTheTopPlayerController::OnSetDestinationReleased()
{
	// If it was a short press
	if (FollowTime <= ShortPressThreshold)
	{
		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}

	FollowTime = 0.0f;

	if (!GetWorldTimerManager().IsTimerActive(AIControlActivateTimer))
	{
		GetWorldTimerManager().SetTimer(AIControlActivateTimer,
			this,
			&AIdleShopInTheTopPlayerController::StartAIControl,
			TimeToActivateAIControl,
			true);
	}
}

// Triggered every frame when the input is held down
void AIdleShopInTheTopPlayerController::OnTouchTriggered()
{
	bIsTouch = true;
	OnSetDestinationTriggered();
}

void AIdleShopInTheTopPlayerController::OnTouchReleased()
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void AIdleShopInTheTopPlayerController::StartAIControl()
{
	UE_LOG(LogTemp, Warning, TEXT("StartAIControl"));
	if (!Character)
	{
		Character = Cast<AIdleShopInTheTopCharacter>(GetCharacter());
	}

	if (Character)
	{
		Character->StartAIControl();
	}
}

void AIdleShopInTheTopPlayerController::StopAIControl()
{
	UE_LOG(LogTemp, Warning, TEXT("StopAIControl"));
	GetWorldTimerManager().ClearTimer(AIControlActivateTimer);
	if (Character)
	{
		Character->bAIControlled = false;
		Character->StopAIControl();
	}
}
